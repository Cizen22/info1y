#include <stdio.h>
#include <stdlib.h> // srand, rand
#include <time.h>// time

int interoCasuale(int N);
int leggiNumeroInteroPositivo();

int main()
{
    int M, i, somma = 0, lancio;
    M = leggiNumeroInteroPositivo();

    srand(time(NULL)); //serve per far generare numeri diversi ogni volta alla funzione rand. La metto fuori dal loop (o dalla funzione) altrimenti avrei sempre lo stesso numero generato

    for(i=0; i<M; i++) {
        lancio = interoCasuale(6); //6 perchè un dado a 6 facce
        printf("Il lancio del dado %d è %d\n", i+1, lancio); //faccio 1+i solo per far stampare il numero di dado partendo da 1 e non da 0
        somma = somma + lancio;
    }

    printf("La somma dei lanci è %d\n", somma);

    return 0;
}

int interoCasuale(int N) {
    return (rand()%N+1); //genera un numero fra 0 e N
}

int leggiNumeroInteroPositivo() {

    float num_orig;
    int num;

    do {
        printf("Insersci il numero di dai (numero interno positivo): ");
        scanf("%f%*c", &num_orig);
        num = num_orig;
    } while(num < 1 || num != num_orig); //cicla finchè non inseriamo un numero positivo e interno

    return num;
}
