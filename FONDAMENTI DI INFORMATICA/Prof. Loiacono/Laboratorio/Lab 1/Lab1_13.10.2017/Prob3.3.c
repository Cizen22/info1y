#include <stdio.h>

int main()
{
    float A, B;
    char operazione;
    printf("Inserire due numeri:\n");
    scanf("%f%f%*c", &A, &B);
    printf("Inserire l'operazione desiderata (+, -, *, /):\n");
    scanf("%c", &operazione);

    switch(operazione)
    {
        case '+': printf ("Risulato: %f\n", A+B); break;
        case '-': printf ("Risulato: %f\n", A-B); break;
        case '*': printf ("Risulato: %f\n", A*B); break;
        case '/': printf ("Risulato: %f\n", A/B); break;
        default: printf("Operazione non valida\n");
    }

    return 0;
}
