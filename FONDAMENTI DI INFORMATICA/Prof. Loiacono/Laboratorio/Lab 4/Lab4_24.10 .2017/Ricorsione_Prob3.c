/*

Scrivere una funzione ricorsiva che, dato un numero intero N, stampa a video la sequenza “+*|*+”, dove * corrisponde ad un numero i=0,1,...N di segni ‘-’.
Creare quindi una ulteriore funzione, che riceve in ingresso un intero M (maggiore o uguale a 1) e utilizza la funzione precedente per stampare un albero di Natale di M righe.
Ad esempio l’output per M=4 dovrà essere:
   +
  +|+
 +-|-+
+--|--+

*/

#include <stdio.h>

int leggiNumeroInteroPositivo();
void stampa(int status, int N, int orig);
void albero(int n);

int main()
{

    albero(leggiNumeroInteroPositivo());
    return 0;
}

void albero(int n) {
    printf("%*s+\n", n-1, ""); //stampa n-1 spazi e poi un + (è la prima linea. Gli spazi servono per centrare il +)
    for(int i=0; i<n-1; i++) {
        printf("%*s", n-i-2, ""); //metto un numero variabile di spazi a seconda di quante sono le linee dell'albero
        stampa(1, i, i);
    }
}

void stampa(int status, int N, int orig) {

    if(status == 1) { //mette il primo "+"
        printf("+");
        status = 2;
    }

    if(status == 2 && N != 0) { //mette la prima sequenza di "-"
        printf("-");
        stampa(2, N-1, orig);
    } else if (status == 2 && N == 0) { //mette " | "
        printf("|");
        stampa(3, orig, orig);
    }

    if(status == 3 && N != 0) { //mette la seconda sequenza di "-"
        printf("-");
        stampa(3, N-1, orig);
    } else if (status == 3 && N == 0) { //mette il "+" finale e va a capo
        printf("+\n");
    }

}

int leggiNumeroInteroPositivo() {

    float num_orig;
    int num;

    do {
        printf("Quanto deve essere alto l'albero?\n");
        scanf("%f%*c", &num_orig);
        num = num_orig;
    } while(num < 1 || num != num_orig); //cicla finchè non inseriamo un numero positivo e interno

    return num;
}
