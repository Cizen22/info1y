/*

Scrivere un programma che legge una matrice quadrata di dimensione specificata dall'utente (al massimo 10 righe e 10 colonne).
Quindi calcoli e stampi la somma dei valori sulla diagonale principale, la somma dei valori sopra la diagonale principale e la somma dei valori
sotto la diagonale principale

*/

#include <stdio.h>

#define MAX_DIM 10


int main()
{
    int n, r, c, somma_diag = 0, somma_sopra_diag = 0, somma_sotto_diag = 0;

    double matrice[MAX_DIM][MAX_DIM];

    do {
        printf("Qual è la dimensione della matrice quadrata che vuoi creare? ");
        scanf("%d", &n);
    } while (n<1||n>10);

    printf("Inserisci ora i valori della matrice %dx%d:\n", n, n);

    for (r=0;r<n;r++)
        for (c=0;c<n;c++) {
            printf("Riga %d - Colonna %d\n", r, c);
            scanf("%lf",&matrice[r][c]);
        }

    for(r=0; r<n; r++)
        somma_diag = somma_diag + matrice[r][r];

    for(r=0; r<n-1; r++)
        for(c=r+1; c<n; c++)
        somma_sopra_diag = somma_sopra_diag + matrice[r][c];

    for(c=0; c<n-1; c++)
        for(r=c+1; r<n; r++)
        somma_sotto_diag = somma_sotto_diag + matrice[r][c];

    printf("La somma dei valori sulla diagonale principale è: %d\n", somma_diag);
    printf("La somma dei valori sopra la diagonale principale è: %d\n", somma_sopra_diag);
    printf("La somma dei valori sotto la diagonale principale è: %d\n", somma_sotto_diag);

    return 0;
}
