#include <stdio.h>
#include <math.h>

#define N 8

int main(){
	
	int i=0, k=0, w=0;
	int v1[N] = {3, -3, 5, 6, 2, 4, 2, 11};
	int v2[N] = {5,-6,7,6,2,4,2,9};
	
	printf("----Vettori ciclici----\n");
	
	
	for(i=0; v2[i]>=0 && v2[i]<=N-1 && w!=1; ){
		i=v2[i];
		k++;
		if(k>N){
			w=1;
		}
	}
	
	printf("Il vettore contiene i valori =");
	for(i=0; v2[i]<N; i++)
		printf(" %d", v2[i]);
	
	printf("\n");
	
	if(w==0){
		printf("Il vettore non e' ciclico");
	}
	else
		printf("Il vettore e' ciclico");
	
	return 0;
}
