#include <stdio.h>
#include <math.h>

int main(){
	
	int num=0, a=1, b=1, c=1, Pa=1, Pb=1, Pc=1, max=0;
	printf("----Scomposizione in somma di quadrati----\n");
	printf("Inserisci un numero: ");
	scanf("%d", &num);
	max=sqrt((num/2)+1);
	
	while(a<num){
		Pa=pow(a,2);
		while(b<num){
			Pb=pow(b,2);
			if(num==Pa+Pb){
				printf("%d ==> %d = %d + %d = %d^2 + %d^2\n", num, num, Pa, Pb, a, b);
			}
			b++;
		}
		a++;
		b=1;
	}
	
	printf("\n\nSomma di 3 quadrati: \n");
	 
	a=1;
	b=1;
	Pa=1; 
	Pb=1;
		while(a<num){
			Pa=pow(a,2);
			while(b<num){
				Pb=pow(b,2);
				while(c<num){
					Pc=pow(c, 2);
					if(num==Pa+Pb+Pc){
						printf("%d ==> %d = %d + %d + %d = %d^2 + %d^2 + %d^2\n", num, num, Pa, Pb, Pc, a, b, c);
					}
					c++;
				}
				b++;
			}
		a++;
		b=1;
		c=1;
	}
	
	
	
	
	return 0;
}
