#include <stdio.h>
#include <string.h>



int palindromo (char * p) 
{
    
    for(int i = 0; i < strlen(p) / 2; i++)
	{
        if(p[i] != p[(strlen(p)-1) - i])
		{
                return 0;
        }
		
    }
    
    return 1;
}

int transp (char * p) 
{

	char par [strlen(p)];
	
	for(int i = 0;i<strlen(p);i++)
	{
		par[i] = p[i];
		if (i==(strlen(p)-1))
		{
			par[strlen(p)-1] = '\0';
		}
		
	}
	
	if (palindromo(par)) 
	{
		return 1;	
	}
	
	for(int i=0;i<strlen(p);i++)
	{
		par[i] = p[i+1];
	}
	
	if (palindromo(par))
	{
		return 1;	
	}
	
	return 0;
}

int main(void) 
{
	
	char * par;
	
	printf("Insert string: ");
	scanf("%s",par);
	
	printf("Checking if palidrome...\n");
	
	if(palindromo(par) || transp(par))
	{
		
		printf("%s is palindrome\n",par);
		
	}
	
	else 
	{
		
		printf("%s is NOT palindrome\n",par);
		
	}
	
	
}
	

