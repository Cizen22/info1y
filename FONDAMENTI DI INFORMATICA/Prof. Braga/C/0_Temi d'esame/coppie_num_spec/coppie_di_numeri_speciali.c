#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define max 100

int dppqp (int n1, int n2);
int decimal(float num);

int main (void)
{

    int v1 [max]; //= {2, 50, 13, 16, 8, 7, 8, 2, 18, 6, 6, 16, 4, 1, 25, 0};
    int dim = 0;

    printf("Quanti numeri vuoi nella lista? : ");
    scanf("%d",&dim);

    *v1 = (int)malloc(dim*sizeof(int));

    for(int i = 0; i < dim; i++)
    {
        printf("Inserire %d-esimo elemento: ",i);
        scanf("%d", &v1[i]);
    }

    for(int j = 0; j < (dim-1); j++)
    {

        if(dppqp(v1[j],v1[j+1]) == 1)
        {
            printf("%d e %d sono una coppia di numeri speciali\n",v1[j],v1[j+1]);
        }

    }

}

int dppqp (int n1, int n2)
{

    if((n1 != n2) && ((n1%2 == 0) && (n2%2 == 0)) && (decimal(sqrt(n1*n2)) == 0))
    {
        return 1;
    }

    else
    {
        return 0;
    }
}

int decimal(float num)
{
    int verif;
    verif = num;
    if (num == verif) 
    {
        return 0;
    }
    else 
    {
        return 1;
    }
}